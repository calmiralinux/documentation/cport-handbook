# cport handbook

## Dependencies

- `rustc`, `cargo`
- `mdbook`

### Install dependencies

```bash
cargo install mdbook
export PATH=${PATH}:${HOME}/.cargo/bin
```

## Build

```bash
mdbook build
```

## Serve

```bash
mdbook serve --open
```
