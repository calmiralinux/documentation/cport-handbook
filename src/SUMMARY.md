# Содержание

- [Введение](intro.md) <!-- рассказать о проекте cport. какие программы содержит и для чего предназначена каждая из них -->
- [Предназначение системы портов](ports_intro.md) <!-- что такое система портов. какие задачи она решает -->
- [Строение системы портов](ports/README.md)
  - [Структура директорий. (Под)категории, директории портов](ports/dirs.md)
  - [Файлы в директории порта](ports/files.md)
    - [Строение `install`](ports/install_structure.md)
    - [Строение `port.toml`](ports/port_toml_structure.md)
    - [Строение `files.list`](ports/files_list_structure.md)
    - [Строение `message`](ports/message_structure.md)
    - [Строение `config`](ports/config_structure.md)
    - [Строение `description`](ports/descr_structure.md)
    - [Дополнительные конфиги и патчи](ports/port_files_dir.md)
- [Управление программным обеспечением](software_manage/README.md)
  - [Применение `cport`](software_manage/cport.md)
  - [Установка программного обеспечения](software_manage/install.md)
  - [Удаление программного обеспечения](software_manage/remove.md)
  - [Просмотр информации о программном обеспечении](software_manage/info.md)
<!-- - [Использование системы сборки дистрибутива](bs/README.md)
  - [Настройка системы сборки](bs/configure.md)
  - [Создание режимов сборки](bs/work_modes.md)
  - [Исполнение сборочных инструкций](bs/building.md)
- [Создание новых портов](create_ports/README.md)
  - [Введение в `git`](create_ports/git.md)
  - [Использование `mkport`](create_ports/mkport.md)
  - [Проверка актуальности портов (`poc`)](create_ports/poc.md)
  - [Обновление файлов портов](create_ports/pu.md) -->
