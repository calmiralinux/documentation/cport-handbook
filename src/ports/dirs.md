# Структура директорий. (Под)категории, директории портов

Типичная структура файлов в `/usr/ports`:

```
/usr/ports
├── base
│  ├── acl
│  │  ├── files.list
│  │  ├── install
│  │  ├── port.toml
│  │  └── README.md
│  ├── attr
│  │  ├── files.list
│  │  ├── install
│  │  ├── port.toml
│  │  └── README.md
│  ├── bison
│  │  ├── files.list
│  │  ├── install
│  │  ├── port.toml
│  │  └── README.md
│  ├── bzip2
│  │  ├── files
│  │  │  └── bzip2-1.0.8-install_docs-1.patch
│  │  ├── files.list
│  │  ├── install
│  │  ├── port.toml
│  │  └── README.md
├── pst
│  ├── docbook-xml-4.5
│  │  ├── files.list
│  │  ├── install
│  │  ├── port.toml
│  │  └── README.md
│  ├── docbook-xsl
│  │  ├── files
│  │  │  └── docbook-xsl-nons-1.79.2-stack_fix-1.patch
│  │  ├── files.list
│  │  ├── install
│  │  └── port.toml
└── xorg
   ├── wm
   │  ├── fluxbox
   │  │  ├── files.list
   │  │  ├── install
   │  │  ├── port.toml
   │  │  └── README.md
   │  └── icewm
   │     ├── files.list
   │     ├── install
   │     ├── port.toml
   │     └── README.md
   ├── x11-drivers
   │  ├── libva
   │  │  ├── install
   │  │  ├── port.toml
   │  │  └── README.md
   │  └── libvdpau
   │     ├── install
   │     ├── port.toml
   │     └── README.md
   ├── x11-libs
   │  ├── at-spi2-atk
   │  │  ├── files.list
   │  │  ├── install
   │  │  ├── port.toml
   │  │  └── README.md
   │  ├── at-spi2-core
   │  │  ├── files.list
   │  │  ├── install
   │  │  ├── port.toml
   │  │  └── README.md
   │  ├── atk
   │  │  ├── files.list
   │  │  ├── install
   │  │  ├── port.toml
   │  │  └── README.md
```

В `/usr/ports` находится несколько каталогов: `base/`, `general/`, `editors/`, `xorg/`. Каждая из них является категорией программного обеспечения. В этих каталогах расположены подкаталоги. В зависимости от их содержимого, они могут являться либо подкатегориями программного обеспечения, либо непосредственно самими портами. Подкатегории содержат сами порты, а в директориях портов обязательно располагаются текстовые файлы `install` и `port.toml`, хотя большинство портов включают в себя и другие файлы, которые являются опциональными и для `cport`, как правило, ничего не означают.
