# Строение files.list

`files.list` содержит список файлов, которые порт устанавливает в систему. Каждый файл порта указывается здесь на новой строке.

- **Статус файла:** опциональный

## Пример файла `files.list`

Пример взят из порта `base/wget` (частично):

```
/etc/wgetrc
/usr/share/locale/rw/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/ms/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/af/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/zh_TW/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/zh_TW/LC_MESSAGES/wget.mo
/usr/share/locale/zh_CN/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/zh_CN/LC_MESSAGES/wget.mo
/usr/share/locale/vi/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/vi/LC_MESSAGES/wget.mo
/usr/share/locale/uk/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/uk/LC_MESSAGES/wget.mo
/usr/share/locale/tr/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/tr/LC_MESSAGES/wget.mo
/usr/share/locale/sv/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/sv/LC_MESSAGES/wget.mo
/usr/share/locale/sr/LC_MESSAGES/wget-gnulib.mo
/usr/share/locale/sr/LC_MESSAGES/wget.mo
/usr/share/locale/sl/LC_MESSAGES/wget-gnulib.mo
/usr/share/info/dir
/usr/share/info/wget.info
/usr/share/man/man1/wget.1
/usr/bin/wget
```
