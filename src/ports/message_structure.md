# Строение message

`message` — текстовый файл, содержащий какое-либо уведомление или сообщение иного характера, которое будет **явно** отображено пользователю после сборки порта.

- **Статус файла:** опциональный

## Пример файла `message`

Пример взят из порта `postcpl/linux-pam`:

```
'base/shadow' port needs to be rebuilded after installing and
configuring '$PORT_NAME'.

With $NAME-$VERSION and higher, the pam_cracklib module is not installed by
default. To enforce strong passwords, it is recommended to use
libpwquality-1.4.5.

-------------------------------------------------------------------------------
If you have a system with $NAME installed and working, be careful when
modifying the files in /etc/pam.d, since your system may become totally
unusable. If you want to run the tests, you do not need to create another
/etc/pam.d/other file. The installed one can be used for that purpose.

You should also be aware that make install overwrites the configuration files
in /etc/security as well as /etc/environment. In case you have modified those
files, be sure to back them up.
```

## Строение `message`

Данный файл содержит текст в кодировке UTF-8. Настоятельно рекомендуется на каждой строке умещать **не более 80 символов** для того, чтобы текст корректно отображался в псевдографическом окне, которое выводит `cport` для отображения содержимого данного файла.

![](pic/view_message.png)

В тексте `message` допустимо использование следующих переменных, значения которых будут автоматически подставлены перед выводом содержимого файла пользователю:

- **`$PORT_NAME`** - имя порта (например, `base/acl`).
- **`$NAME`** - имя пакета, взятое из параметра `package.name` файла `port.toml` порта (например, `acl`).
- **`$VERSION`** - версия пакета, взятая из параметра `package.version` файла `port.toml` порта (например, `2.0`).
- **`$PORT_DIR`** - путь до директории с портом в файловой системе (например, `/usr/ports/base/acl`).
