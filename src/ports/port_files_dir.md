# Дополнительные конфиги и патчи

В директории порта может распологаться *опциональная* поддиректория `files/`. Несмотря на то, что количество, тип и размер файлов в этой поддиректории никак не регулируется, рекомендуется хранить там только текстовые файлы: тексты лицензий, патчи или конфиги.

## Примерное строение

```
/usr/ports/net/lynx/files/lynx-2.8.9rel.1-security_fix-1.patch
/usr/ports/base/gcc/files/gcc-12.1.0-glibc_2.36-1.patch
/usr/ports/base/grub/files/grub-2.06-upstream_fixes-1.patch
/usr/ports/base/zstd/files/zstd-1.5.2.patch
/usr/ports/base/kbd/files/kbd-2.6.0-backspace-1.patch
/usr/ports/base/kernels/master_libre/files/config-6.3.7
/usr/ports/base/kernels/master/files/config-6.3.7
/usr/ports/base/glibc/files/tzdata2023c.tar.gz
```

## Примеры использования

Пример взят из порта `base/kbd`:

```bash
# Фрагмент файла `install` из директории порта `base/kbd`

NAME="kbd"
VERSION="2.6.0"
ARCHIVE="${NAME}-${VERSION}.tar.gz"

function prepare() {
  # Путь до директории `files/`: ${PORT_DIR}/files/
  patch -Np1 -i ${PORT_DIR}/files/kbd-${VERSION}-backspace-1.patch

...
```

Пример взят из порта `base/glibc`:

```bash
# Фрагмент файла `install` из директории порта `base/glibc`

NAME="glibc"
VERSION="2.37"
ARCHIVE="${NAME}-${VERSION}.tar.xz"

function prepare() {
  cp ${PORT_DIR}/files/glibc-${VERSION}-fhs-1.patch .
  cp ${PORT_DIR}/files/tzdata2023c.tar.gz .

  patch -Np1 -i glibc-${VERSION}-fhs-1.patch

...
}

...

function postinst() {
...

  tar -xf tzdata2023c.tar.gz

  ZONEINFO=/usr/share/zoneinfo
  mkdir -pv $ZONEINFO/{posix,right}

...
}
```
