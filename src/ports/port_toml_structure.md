# Строение port.toml

`port.toml` является первым файлом, который читает `cport` для получения сведений о порте. Этот файл разбит на 3 секции, одна из которых обязательная, а две остальные — опциональные.

- **Статус файла:** необходимый

## Пример файла `port.toml`

Пример взят из порта `base/wget`

```toml
[package]
name = "wget"
version = "1.21.4"
description = "The Wget package contains a utility useful for non-interactive downloading of files from the Web"
maintainers = ["Ivan Repyakh <red-122@mail.ru>"]
releases = ["v2.0a1", "v2.0a2", "v2.0a3"]
priority = "system"
usage = 45.0
upgrade_mode = "hard"
build_time = 0.1

[deps]
required = ["bae/make-ca", "base/openssl", "base/pcre", "net/libpsl"]
optional = ["general/libidn2"]

[port]
url = "https://ftp.gnu.org/gnu/wget/wget-1.21.4.tar.gz"
md5 = "e7f7ca2f215b711f76584756ebd3c853"
sha256 = "81542f5cefb8faacc39bbbc6c82ded80e3e4a88505ae72ea51df27525bcde04c"
```

## Секции `port.toml`

1. `package` описывает информацию о пакете: его *имя* (имя *порта* - `base/wget`, имя *пакета* - `wget`), версия, краткое описание, список сопровождающих[^1] этот порт, приоритет и иные вещи. Эти сведения используются как внутри программы `cport`, так и конкретным пользователем.
2. `deps` - опциональная секция, которая содержит список зависимостей порта. В случае, если зависимостей нет, то эта секция не должна быть указана.
3. `port` содержит параметры исключительно для `cport`: ссылка для скачивания архива с исходным кодом порта и контрольные суммы для проверки корректности скачивания. Если порт не содержит исходного кода и ему нечего скачивать, то эта секция также не указывается.

## Параметры в `port.toml`

### Секция `package`

| Параметр | Тип данных Rust | Описание |
|----------|-----------------|----------|
| `name` | `String` | Имя *пакета* |
| `version` | `String` | Версия пакета/порта |
| `description` | `String` | Краткое описание порта (одна строка, рекомендуется не более 80 символов) |
| `maintainers` | `Vec<String>` | Список сопровождающих данный порт |
| `priority` | `String` | Приоритет порта (`system`/`user`) |
| `usage` | `f32` | Сколько места будет занимать порт в файловой системе после установки |
| `upgrade_mode` | `String` | Режим обновления порта (`soft`/`hard`) |
| `build_time` | `f32` | Относительное время сборки порта |

### Секция `deps`

| Параметр    | Тип данных Rust       | Описание                        |
|-------------|-----------------------|---------------------------------|
| `required`  | `Option<Vec<String>>` | Необходимые зависимости порта   |
| `recommend` | `Option<Vec<String>>` | Рекомендуемые зависимости порта |
| `optional`  | `Option<Vec<String>>` | Опциональные зависимости порта  |

### Секция `port`

| Параметр | Тип данных Rust | Описание |
|----------|-----------------|----------|
| `url` | `Option<String>` | Ссылка для скачивания архива с исходным кодом порта |
| `file` | `Option<String>` | Имя скачанного файла (используется, когда невозможно вычислить имя файла из параметра `port.url`) |
| `md5` | `Option<String>` | Контрольная MD5-сумма |
| `sha256` | `Option<String>` | Контрольная SHA256-сумма |

---

[^1]: *Сопровождающий* - человек, который создал порт и/или участвует в работе над ним: создаёт или правит сборочные инструкции, поддерживает порт в актуальном состоянии и т.д.
