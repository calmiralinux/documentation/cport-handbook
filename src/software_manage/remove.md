# Удаление программного обеспечения

![](pic/remove.png)

Удаление программного обеспечения — это одна из важных проблем, которую решает `cport`. Установка достаточно проста, но с удалением собранного из исходного кода свободного ПО часто возникают проблемы, так как на жёстком диске ПК придётся хранить архив с исходным кодом пакета, в котором содержится `Makefile` или иные файлы, содержащие инструкции как по сборке и установке пакета, так и для его удаления из системы. Но есть один нюанс: не все разработчики пакетов добавляют инструкции по удалению своего ПО из системы. Тогда пользователю придётся решать эту проблему самостоятельно: находить все файлы, которые принадлежат нужному пакету, и вручную удалять их.

Но что если пользователь не удалит какие-то файлы до конца? Что если пользователь удалит не те файлы? Те, которые принадлежат другому пакету? В таком случае он может вообще сломать либо какое-то другое ПО, либо вообще всю систему. Для того, чтобы обезопасить пользователя, и была реализована функция удаления ПО из системы средствами `cport`, а не средствами, про которые многие разработчики забывают или принципиально не добавляют.

## Принцип работы

Если в директории порта содержится *опциональный* файл `files.list`, то удаление этого порта из системы возможно. `cport` читает `files.list`, получает из него список файлов для удаления и делает попытки удалить их из системы.

## Синтаксис

```bash
cport remove [список портов для удаления] {опциональные ключи}
```

### Пример

```bash
cport remove editors/gvim general/{cpio,elogind} -y
```

Команда выше удаляет порты `editors/gvim`, `general/cpio` и `general/elogind` из системы.

> ## О разделении портов по приоритету
>
> Порты могут иметь какой-то из двух приоритетов:
> 1. **Системный** — такие порты отвечают за корректную работу дистрибутива Calmira GNU/Linux-libre, а поэтому не могут быть удаленя из системы средствами `cport`.
> 2. **Пользовательский** — с такими портами человек может делать всё, что посчитает нужным: устанавливать, обновлять и удалять без каких-либо ограничений со стороны `cport`.
>
> Системные порты содержатся, как правило, в категории `base/`. Их удаление из системы запрещено.
>
> ## Об удалении файлов
>
> `cport` способен удалять только *файлы* и *ссылки на файлы*. Каталоги, принадлежащие к удаляемому порту, в целях безопасности не удаляются. Кроме того, во второй версии стандарта системы портов в `files.list` упоминание каталогов исключено вообще.

## Удаление порта из альтернативного префикса установки

По умолчанию все порты устанавливаются в `/`. Однако при установке вы с помощью ключа `--destination`/`-d` могли изменить префикс установки на нужный вам. По аналогии с установкой, для опции `remove` также есть ключ `--destination`/`-d`, позволяющий указать путь с файлами установленного порта:

```bash
cport remove general/git editors/gvim -d /tmp/software
```

Эта команда удалит порты `general/git` и `editors/gvim` из директории `/tmp/software`.

## Подробный вывод

При удалении `cport` не будет показывать, какие файлы были удалены. В случае, если вы хотите видеть, *что именно* было удалено, используйте ключ `--verbose`/`-v`:

```bash
cport remove general/git -v
```

## Запросы продолжения

Перед удалением порта `cport` также запрашивает продолжение операции. В том случае, если вы не хотите или вам не требуется отвечать на все из них, то можете использовать ключ `--yes`/`-y` для автоматического утвердительного ответа на все запросы продолжения.

```bash
cport remove general/git -y
```
